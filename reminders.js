let chores = require('./data/chores.js')
let schedule = require('node-schedule')
let controller = require('./app.js')

let devChannelWebhook = 'https://hooks.slack.com/services/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'
let choreChannelWebhook = 'https://hooks.slack.com/services/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'

// To be used to send chore assignments and occaisional chore reminders throughout the week
// At the end of the week, this bot will send a "report"
var bot = controller.spawn({
  incoming_webhook: {
    url: devChannelWebhook
  }
})

// Rotate and assign chores at 7pm on Mondays
schedule.scheduleJob('0 19 * * 1', () => {
  let users = rotateRoomAssignments()

  users.names.forEach((name) => {
    bot.sendWebhook(
      assignChores(users[name], chores.chores[users[name].assignedRoom]),
      logErrorCallback
    )
  })
})


/**
 *
 * Helper functions
 *
 */

function assignChores(user, room) {
  let d = new Date()
  let nowTs = unixTs(d)
  d.setDate(d.getDate() + 6)
  let weekTs = unixTs(d)

  let titleAttachment = {
    title: `You've been assigned the ${room.name}!`,
    color: 'good',
    fields: [
      {
        title: 'Room',
        value: room.name,
        short: true
      },
      {
        title: 'Assigned to',
        value: user.name,
        short: true
      },
      {
        title: 'Start Date',
        value: `<!date^${nowTs}^{date_long}|f>`,
        short: true
      },
      {
        title: 'Due Date',
        value: `<!date^${weekTs}^{date_long}|f>`,
        short: true
      }
    ]
  }

  let attachments = taskAttachments(room)
  attachments.unshift(titleAttachment)

  return {
    text: `<@${user.id}> I've assigned you some chores for the week!`,
    attachments: attachments
  }
}

function rotateRoomAssignments() {
  var users = require('./data/users.json')
  let fs = require('fs')

  let lastUser = users.names[users.names.length - 1]
  var lastAssignedRoom = users[lastUser].assignedRoom
  for (let i = users.names.length - 2; i >= 0 ; i--) {
    var name1 = users.names[i]
    var name2 = users.names[i + 1]

    users[name2].assignedRoom = users[name1].assignedRoom
  }
  let firstUser = users.names[0]
  users[firstUser].assignedRoom = lastAssignedRoom

  fs.writeFile('data/users.json', JSON.stringify(users), 'utf8', logErrorCallback)

  return users
}

function logErrorCallback(err) {
  if (err) {
    console.log(err)
  }
}

function unixTs(date) {
  return Math.floor(date / 1000)
}

function taskAttachments(choreRoom) {
  return choreRoom.taskNames.map(name => {
    return {
      title: name,
      color: 'warning',
      callback_id: choreRoom.name,
      text: choreRoom.tasks[name],
      actions: [
        {
          text: 'Mark task as finished',
          name: name,
          type: 'button',
          value: true
        }
      ]
    }
  })
}

function taskList(choreRoom) {
  var tasks = choreRoom.taskNames.map(name => {
    return `${name}: ${choreRoom.tasks[name]}`
  })
  tasks[0] = '• ' + tasks[0]
  tasks = tasks.join('\n• ')
  return tasks
}