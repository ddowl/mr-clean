module.exports = {
  drew: {
    id: 'U6R494MQ8',
    name: 'drewbzies',
    assignedRoom: 'kitchen'
  },
  gina: {
    id: 'U6SS4S547',
    name: 'gdh',
    assignedRoom: 'bathroom'
  },
  jenna: {
    id: 'U6R7SQK9P',
    name: 'jenna',
    assignedRoom: 'livingRoom'
  },
  names: ['drew', 'gina', 'jenna']
}