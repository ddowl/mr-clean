const KITCHEN = 'kitchen'
const BATHROOM = 'bathroom'
const LIVING_ROOM = 'living room'

function sweep(room) {
  return { sweep: `Sweep the ${room} floor.` }
}

function mop(room) {
  return {
    mop: `Mop the ${room} floor. Remember to refill the water tank with distilled water!`
  }
}

function counters(room) {
  return {
    counters: `Wipe down the counters in the ${room}. Use general cleaner under the sink.`
  }
}

function sinkItem(item) {
  return `${item} under the sink.`
}

module.exports = {
  chores: {
    kitchen: {
      id: 0,
      name: KITCHEN,
      taskNames: ['counters', 'dishes', 'sweep', 'mop'],
      tasks: Object.assign(
        counters(KITCHEN),
        { dishes: 'Put clean dishes from the drying rack into their appropriate places' },
        sweep(KITCHEN),
        mop(KITCHEN)
      )
    },
    bathroom: {
      id: 1,
      name: BATHROOM,
      taskNames: ['counters', 'sweep', 'mop', 'toilet', 'shower'],
      tasks: Object.assign(
        counters(BATHROOM),
        sweep(BATHROOM),
        mop(BATHROOM),
        { toilet: `Clean the toilet with the ${sinkItem('toilet cleaner')} and the brush next to the toilet.` },
        { shower: `Clean the shower with ${sinkItem('scrubbing bubbles')}` }
      )
    },
    livingRoom: {
      id: 2,
      name: LIVING_ROOM,
      taskNames: ['counters', 'vacuum', 'trash'],      
      tasks: Object.assign(
        counters(LIVING_ROOM),
        { vacuum: 'Vacuum thoroughly. Turn down the brush speed when going over the carpet.' },
        { trash: 'Take out the trash.' }
      )
    }
  },
  rooms: ['kitchen', 'bathroom', 'livingRoom']
}
